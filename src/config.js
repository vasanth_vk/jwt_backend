const dotenv = require("dotenv");
dotenv.config();

module.exports = {
    mysql_db_host: process.env.MYSQL_DB_HOST,
    mysql_db_user: process.env.MYSQL_DB_USER,
    mysql_db_password: process.env.MYSQL_DB_PASSWORD,
    mysql_database_name: process.env.MYSQL_DATABASE_NAME,
    jwt_access_token: process.env.JWT_ACCESS_TOKEN,
    port: process.env.PORT || 5000,
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
};
