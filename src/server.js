const express = require("express");
const path = require("path");
const app = express();
const cors = require("cors");
const { port } = require("./config");
const register = require("./routes/register");
const login = require("./routes/login");
const findall = require("./routes/findall");

app.use(cors());
app.set("json spaces", 4);

app.use(express.json());
app.use(
    express.urlencoded({
        extended: true,
    })
);

app.get("/", (req, res) => {
    res.json({ message: "home" });
});

app.use("/register", register);
app.use("/login", login);
app.use("/findall", findall);
app.use((req, res, next) => {
    res.status(404).json({ message: "NOT FOUND" });
});

app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({ message: "TRY AGIN LATER" });
});

app.listen(port, () => {
    console.log(`Sever listening at ${port}`);
});
