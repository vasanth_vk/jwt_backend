const mysql = require("mysql");
const config = require("../config");

let pool = mysql.createPool({
    connectionLimit: 10,
    host: config.mysql_db_host,
    user: config.mysql_db_user,
    password: config.mysql_db_password,
    database: config.mysql_database_name,
});

module.exports = pool;
