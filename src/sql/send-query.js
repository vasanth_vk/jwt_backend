const config = require("../config");
const mysql = require("mysql");
const pool = require("./mysql-connection");

const sendQuery = (myQuery) => {
    return new Promise((resolve, reject) => {
        pool.getConnection(function (error, connection) {
            if (error) {
                console.log(error);
            } else {
                connection.query(myQuery, (err, results) => {
                    if (err) {
                        connection.release();
                        reject(`${err.message}`);
                    } else {
                        connection.release();
                        resolve(results);
                    }
                });
            }
        });
    });
};

module.exports = sendQuery;

sendQuery("delete from userdata;").then((data) => {
    console.log(data);
});
