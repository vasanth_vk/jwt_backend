const db = require("../models");
const userData = db.userData;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { jwt_access_token } = require("../config");
const createUser = async (req, res) => {
    try {
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        const user = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: hashedPassword,
        };

        let condition = { where: { email: user.email } };
        const userExists = await userData.findOne(condition);
        if (userExists) {
            res.json({ message: "userExists" });
        } else {
            const userCreated = await userData.create(user);
            console.log(userCreated);
            const accessToken = jwt.sign(
                { email: userCreated.email, id: userCreated.id },
                jwt_access_token
            );
            console.log(accessToken);
            res.json({ message: "userCreated", accessToken: accessToken });
        }
    } catch (error) {
        res.status(500).send({
            message:
                error.message || "Some error occurred while retrieving user.",
        });
    }
};

const userLogin = async (req, res) => {
    try {
        console.log(req.body);
        const user = {
            email: req.body.email,
            password: req.body.password,
        };
        let condition = { where: { email: user.email } };
        const userExists = await userData.findOne(condition);
        if (userExists) {
            const passwordMatched = await bcrypt.compare(
                user.password,
                userExists.password
            );
            if (passwordMatched) {
                const accessToken = jwt.sign(
                    { email: userExists.email, id: userExists.id },
                    jwt_access_token
                );
                res.json({ accessToken: accessToken });
            } else {
                res.json({ message: "invalid password" });
            }
        } else {
            res.json({ message: "invalid email" });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send({
            message:
                error.message || "Some error occurred while retrieving user.",
        });
    }
};

const findAllUsers = async (req, res) => {
    try {
        const allUsers = await userData.findAll();
        let useremailandid = allUsers.map((user) => {
            return { id: user.id, user: user.email };
        });
        res.json(useremailandid);
    } catch (error) {
        console.log(error);
        res.status(500).send({
            message:
                error.message || "Some error occurred while retrieving user.",
        });
    }
};

//compare password await bcrypt.compare(password,user.password)

module.exports = { createUser, userLogin, findAllUsers };
