const express = require("express");
const { findAllUsers } = require("../controllers/controller");

const findall = express.Router();

findall.get("/", findAllUsers);

module.exports = findall;
