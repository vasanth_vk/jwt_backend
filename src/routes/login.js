const express = require("express");
const { userLogin } = require("../controllers/controller");
const jwt = require("jsonwebtoken");
const { jwt_access_token } = require("../config");

const login = express.Router();

login.post("/", userLogin);
module.exports = login;

const authenticateToken = (req, res, next) => {
    const authHeaders = req.headers["authorization"];
    const token = authHeaders && authHeaders.split(" ")[1];
    if (token == null) {
        res.json({ authenticated: false });
    } else {
        jwt.verify(token, jwt_access_token, (err, user) => {
            if (err) {
                res.json({ authenticated: false });
            } else {
                res.json({
                    authenticated: true,
                    user: user.email,
                });
            }
        });
    }
};

login.get("/authenticate", authenticateToken);
