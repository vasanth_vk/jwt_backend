const express = require("express");
const { createUser } = require("../controllers/controller");

const register = express.Router();

register.post("/", createUser);

module.exports = register;
